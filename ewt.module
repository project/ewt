<?php
/**
 * Drupal CoovaEWT Modul
 * http://coova.org/wiki/index.php/CoovaEWT
 * Copyright 2008-2009 (c) Coova Technologies, LLC
 * Licensed under the Gnu Public License.
 */

/**
 * Implementation of hook_perm().
 */
function ewt_perm() {
  return array('administer ewt');
}


/**
 * Implementation of hook_help().
 */
function ewt_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      return t("Integrate with CoovaEWT.");
  }
}

/**
 * Implements hook_menu.
 */
function ewt_menu() {
  global $ewtSettings;

  $menu =
    array(
	  //	  'ewt' =>
	  //	  array('page callback' => 'ewt_render',
	  //		'access arguments' => array('access content'),
	  //		'type' => MENU_LOCAL_TASK,
	  //		),

	  'ewt_proxy' =>
	  array('page callback' => 'ewt_proxy',
		'access arguments' => array('access content'),
		'type' => MENU_LOCAL_TASK,
		),

	  'ewt_proxy_png' =>
	  array('page callback' => 'ewt_proxy_png',
		'access arguments' => array('access content'),
		'type' => MENU_LOCAL_TASK,
		),

	  'ewt_proxy_html' =>
	  array('page callback' => 'ewt_proxy_html',
		'access arguments' => array('access content'),
		'type' => MENU_LOCAL_TASK,
		),

	  );

  if (!$ewtSettings || !$ewtSettings['api_url']) 
    $menu['admin/settings/ewt'] =
      array('title' => 'CoovaEWT',
	    'description' => 'Configure EWT Settings',
	    'page callback' => 'drupal_get_form',
	    'page arguments' => array('ewt_admin_settings'),
	    'access arguments' => array('administer site configuration'),
	    'file' => 'admin.settings.inc',
	    );

  return $menu;
}

function ewt_theme() {
  return array('ewt_table' => array('arguments' => array('form' => NULL)),
	       'ewt_pager' => array('offset' => 0, 'count' => 10, 'total' => 0),
	       );
}

function ewt_content($tid = 0, $fmt = 'jshtml') {
  $tid = (int) $tid;
  return theme('ewt_'.$fmt, $tid);
}

function ewt_set_session($v) {
  if (isset($_REQUEST[$v])) $_SESSION[$v] = $_REQUEST[$v];
}

function ewt_get_param($v) {
  if (isset($_REQUEST[$v])) return $_REQUEST[$v];
  return $_SESSION[$v];
}

function ewt_form_instance() {
  $n = $_REQUEST['__ewtforms']++;
  if ($n > 0) return '-'.$n;
  return '';
}

/**
 *  Initialize the module for the request
 */
function ewt_init() {
  global $user;

  session_start(); 
  require_once drupal_get_path('module', 'ewt').'/EWTClient.php';

  if (file_exists('./'. conf_path() .'/ewt.php')) {
    include_once './'. conf_path() .'/ewt.php';
  }

  $js = "var drupalBasePath = '". base_path() ."';\n".
        "var drupalPage = '". $_GET['q'] ."';\n";

  //  if ($user->uid) {
  //    $js .= "var drupalUserId = '". $user->uid ."';\n".
  //           "var drupalUsername = '". $user->name ."';\n";
  //  }

  drupal_add_js($js, 'inline');

  ewt_include_js(false);
}

function ewt_include_js($need = false) {
  if (!$_REQUEST['hasEWT'] &&
      file_exists(drupal_get_path('module', 'ewt').'/com.coova.ewt.Drupal') && 
      ewt_proxy_enabled($need)) {
    $_REQUEST['hasEWT'] = true;
    $path = drupal_get_path('module', 'ewt');
    $module = "com.coova.ewt.Drupal";
    drupal_add_js($path."/$module/$module.nocache.js", 'module', 'footer');
    drupal_add_css($path."/ewt-style.css");
    drupal_add_css($path."/ewt-datePicker.css");
    drupal_add_css($path."/ewt-local-style.css");
  }
}

function ewt_div($service = 'drupal') {
  ewt_include_js(true);
  return '<div id="ewtWidget'.($_REQUEST['ewtwidgets']++).'" class="ewtWidget" service="'.$service.'"></div>';
}

function ewt_wait_div() {
  return '<div id="ewtWait"><img src="'.base_path().drupal_get_path('module','ewt').'/ewt-wait.gif">&nbsp;&nbsp;loading...</div>';
}

function ewt_menu_div($resource = 'drupal-menu') {
  ewt_include_js(true);
  return '<div id="ewtWidget'.($_REQUEST['ewtwidgets']++).'" class="ewtWidget" service="service" resource="'.$resource.'"></div>';
}

function ewt_bkgd_div() {
  return '<div id="popup-bkgd">&nbsp;</div>';
}

function ewt_proxy($tid = 0) {

  if (!ewt_proxy_enabled(true)) 
    return false;

  $action = $_REQUEST['action'];
  $service = $_REQUEST['s'];
  $res = $_REQUEST['res'];

  if ($res != 'help' && !preg_match(ewt_setting('proxy_regex', '/drupal.*/'), $service)) 
    return false;

  $ewt = ewt_client();

  if (!isset($HTTP_RAW_POST_DATA)){
    $HTTP_RAW_POST_DATA = file_get_contents('php://input');
  }

  $post = $HTTP_RAW_POST_DATA;

  if (isset($post) && ($postObject = json_decode($HTTP_RAW_POST_DATA, true))) {
    ewt_drupal_vars($postObject[$service]);
    $post = json_encode($postObject);
  } else {
    $postObject = array($service => array());
    ewt_drupal_vars($postObject[$service]);
    $post = json_encode($postObject);
  }

  $output = $ewt->proxy($post);
  $ewt->close();

  $modout = module_invoke_all('ewt_service', $service, $action, $HTTP_RAW_POST_DATA, $output);

  if ($modout != null) {
    $output = $modout[0];
  }

  drupal_set_header('Content-Type: application/json; charset=utf-8');
  print $output;
}

function ewt_proxy_enabled($need = false) {
  global $user;

  if (ewt_setting('proxy_enabled', '') == 'true') 
    return true;

  if (ewt_setting('proxy_enabled', '') == 'user' && $user->uid) 
    return true;

  if ($need && ewt_setting('proxy_enabled', '') == 'need')
    return true;

  return false;
}

function ewt_proxy_token($token, $add = false) {
  if ($add) {
    if (!is_array($_SESSION['__ewtTokens']))
      $_SESSION['__ewtTokens'] = array($token);
    else if (!in_array($token, $_SESSION['__ewtTokens']))
      $_SESSION['__ewtTokens'][] = $token;
  } else {
    if (is_array($_SESSION['__ewtTokens']))
      return in_array($token, $_SESSION['__ewtTokens']);
    return false;
  }
}

function ewt_proxy_png($tid = 0) {

  $token = $_REQUEST['token'];

  if (!ewt_proxy_enabled(true))
    if (!ewt_proxy_token($token))
      return false;

  if ($_REQUEST['width']) {
    $width = "&width=".$_REQUEST['width'];
  }

  $ewt = ewt_client();

  if (!isset($HTTP_RAW_POST_DATA)){
    $HTTP_RAW_POST_DATA = file_get_contents('php://input');
  }

  $output = 
    $ewt->proxy_url(ewt_setting('api_png_url', 'http://localhost:1900/ewt/images').
		    '?token='.$token.$width."&session=".session_id(), 
		    $HTTP_RAW_POST_DATA);

  $ewt->close();

  drupal_set_header('Content-Type: image/png');
  print $output;
}

function ewt_proxy_html($tid = 0) {

  $prefix = ewt_setting('api_html_url', '/ewt/report');

  $ewturl = str_replace('..', '', $_REQUEST['ewturl']);

  if (!preg_match('/'.str_replace('/', '\\/', $prefix).'.*/', $ewturl)) 
    return false;

  if (!ewt_proxy_enabled(true))
    if (!ewt_proxy_token($token))
      return false;

  $ewt = ewt_client();

  if (!isset($HTTP_RAW_POST_DATA)) {
    $HTTP_RAW_POST_DATA = file_get_contents('php://input');
  }

  $output = $ewt->proxy_url('http://localhost:1900'.$ewturl.'?'.$ewt->urlargs(), $HTTP_RAW_POST_DATA);

  $ewt->close();

  if ($_REQUEST['img']) {
    drupal_set_header('Content-Type: image/'.$_REQUEST['img']);
  } else if ($_REQUEST['fmt']) {
    drupal_set_header('Content-Type: application/'.$_REQUEST['fmt']);
  } else {
    drupal_set_header('Content-Type: text/html');
    $output = str_replace('http://localhost:1900',base_path().'?q=ewt_proxy_html&img=png&ewturl=',$output);
  }

  print $output;
}

function ewt_fetch_media($token, $width, &$metadata) {
  if ($width) $width = "&width=".$width;
  else $width = "";

  $ewt = ewt_client();

  $output = 
    $ewt->get(ewt_setting('api_png_url', 'http://localhost:1900/ewt/images').
	      '?token='.$token.$width.'&session='.session_id());

  $ewt->close();

  return $output;
}

function ewt_save_media($content, $content_type, &$metadata) {

  $ewt = ewt_client();

  $output = 
    $ewt->post(ewt_setting('api_upload_url', 'http://localhost:1900/ewt/upload').
	       '?session='.session_id(), $content, $content_type);

  $ewt->close();

  return json_decode($output, true);
}

function ewt_render($tid = 0) {
  $tid = (int) $tid;

  session_start();

  print theme("page", ewt_content($tid, $_REQUEST['res']));
}

function ewt_user($op, &$edit, &$account, $category = NULL) {
  switch($op) {
  case 'logout':
    //$_SESSION = array();
    //if (isset($_COOKIE[session_name()])) {
    //setcookie(session_name(), '', time()-42000, '/');
    //}
    //session_regenerate_id(true);
    //session_destroy();
    break;
  }
}

function ewt_block($op = 'list', $delta = 0, $edit = array()) {
  global $user;

}

function ewt_setting_bool($name) {
  global $ewtSettings;
  if (isset($ewtSettings) && isset($ewtSettings[$name])) 
    return $ewtSettings[$name] == true;
  return false;
}

function ewt_setting($name, $def = null, $prefix = 'ewt_') {
  global $ewtSettings;
  return (isset($ewtSettings) && isset($ewtSettings[$name])) ? 
    $ewtSettings[$name] : variable_get($prefix.$name, $def);
}

function ewt_client() {
  return new EWTClient(ewt_setting('api_url'), 
		       ewt_setting('api_username'),
		       ewt_setting('api_password'));
}

function ewt_login_user($name, $email = '') {
  global $user;

  $user = user_load(array('name' => $name));

  if (!$user->uid) {
    $roles = array();
    $user_default = 
      array('name' => $name, 
	    'pass' => user_password(), 
	    'init' => db_escape_string($name), 
	    'mail' => $email,
	    'status' => 1,
	    'roles' => $roles);
    
    // Become user 1 to be able to save profile information
    $admin = array('uid'=> 1);
    $user = user_load($admin);
    
    // now save the user and become the new user. 
    $user = user_save("", $user_default);
    
    watchdog("user", t('new user: %n', array('%n' => $user->name)), 
	     WATCHDOG_NOTICE, l(t("edit user"), "admin/user/edit/$user->uid"));
  }

  module_invoke_all('user', 'login', null, $user);

  watchdog('user', t('Session opened for %name.', array('%name' => $user->name)));
}

function ewt_logout() {
  global $user;

  watchdog('user', t('Session closed for %name.', array('%name' => $user->name)));

  session_regenerate_id();
  session_destroy();

  module_invoke_all('user', 'logout', NULL, $user);

  //session_start();

  $user = drupal_anonymous_user();
}

function ewt_drupal_vars(&$data) {
  global $user, $ewtSettings;
  $data['item']['drupalBase']=base_path();
  $data['item']['drupalPage']=$_GET['q'];
  $data['item']['drupalRoles']=$user->roles;
  $data['item']['drupalUsername']=$user->name;
  $data['item']['drupalUserId']=$user->uid;
  if (is_array($ewtSettings))
    $data['item']['drupalRealm']=$ewtSettings['realm'];
}

function ewt_action($action, $data = array(), $service = 'drupalModule') {

  ewt_drupal_vars($data);

  $ewt = ewt_client();
  $output = $ewt->doAction($service, $action, $data);
  $ewt->close();
 
  return $output;
}

function theme_ewt_pager($offset = 0, $limit = 10, $total = 0) {
  $page = $offset / $limit;
  $first = $offset + 1;
  $last = $offset + $limit;
  if ($last > $total) $last = $total;
  if ($page > 0) {
    $pager .= ' '.l('first', $_GET['q']);
    $pager .= ' '.l('previus', $_GET['q'], array('query'=>'page='.($page - 1)));
  }
  $pager .= ' Showing '.$first.'-'.$last.' of '.$total;
  if ($last < $total) {
    $pager .= ' '.l('next', $_GET['q'], array('query'=>'page='.($page + 1)));
    $pager .= ' '.l('last', $_GET['q'], array('query'=>'page='.floor($total / $limit)));
  }
  return $pager;
}

function theme_ewt_table($form) {
  $header = array();
  $rows = array();

  $offset = 0;
  $limit = 10;

  if ($_GET['page']) {
    $offset = $limit * $_GET['page'];
  }

  //
  //var_dump($form);

  foreach ($form['#value']['input'] as $input) {
    $header[] = $input['label'];
  }

  $table = $form['#value']['table'];
  $service = $form['#value']['service'];
  if (!$service) $service = 'table';

  $ewt = ewt_client();
  $ewt_data = $ewt->getTableRowsSrv($service, $table, $offset, $limit, '', false, true);

  $ewt_cnt = $ewt_data['count'];
  $ewt_rows = $ewt_data[$table];

  //  var_dump($ewt_rows);

  if (is_array($ewt_rows)) {
    foreach ($ewt_rows as $row) {
      $data = array();
      
      foreach ($form['#value']['input'] as $input) {
	$value = '';
	if ($input['key']) {
	  $value = $row[$input['key'].'_display'];
	  if (!$value) $value = $row[$input['key']];
	}
	$data[] = $value;
      }
      
      $rows[] = array('data' => $data);
    }
  }

  $attributes = is_array($form['#attributes']) ? $form['#attributes'] : array();

  $content  = theme('ewt_pager', $offset, $limit, $ewt_cnt);
  $content .= theme('table', $header, $rows, $attributes, $form['caption']);
  return $content;
}

function _ewtwalk(&$form, &$values, &$data, $inst) {
  $cnt = 0;
  if (is_array($data['input'])) {
    foreach ($data['input'] as $input) {
      _walkinput($form, $values, $input, $cnt, $inst);
    }
  }
}

function _walkinput(&$form, &$values, &$input, &$cnt, $inst = '', $msg2drupal = false) {
  $key   = $input['key'];
  $title = $input['label'];
  $value = $input['value'];
  
  $type = false;
  $size = false;
  $attributes = false;
  $options = false;
  $walkinput = false;
  $element = array();

  switch($input['type']) {
    
  case 'table':
    $type = 'markup';
    //$value .= '<table class="ewtTable">';
    $data = $input['data'];

    if ($input['styleName']) 
      $tableStyle = ' class="'.$input['styleName'].'"';

    $cols = -1;
    $rows = sizeof($data);
    for ($row = 0; $row < $rows; $row++) {

      if (!is_array($data[$row])) continue;

      if ($cols < 0)
	$cols = sizeof($data[$row]);

      for ($col = 0; $col < $cols; $col++) {

	$nform = array('#type'=>'fieldset','#tree'=>TRUE);
	
        if (is_array($data[$row][$col])) {
	  foreach ($data[$row][$col] as $cell) {
	    _walkinput($nform, $values, $cell, $cnt, $inst);
	  }
	}

	if ($col == 0) {
	  if ($row == 0) {
	    //	    var_dump('rows='.$rows.' cols='.$cols);
	    $nform['#prefix'] .= '<table'.$tableStyle.'>';
	  }
	  $nform['#prefix'] .= '<tr>';
	} 
	
	//	var_dump('row='.$row.' col='.$col.' '.$nform['#type']);
	
	$nform['#default_value'] = 'hi';
	if (!$nform['#type']) {
	  $nform['#type'] = 'markup';
	}

	$colspan='';
	if ($cell['cols']) {
	  $colspan=' colspan="'.$cell['cols'].'"';
	  $col += $cell['cols'] - 1;
	}
	
	$nform['#prefix'] .= '<td'.$colspan.'>';
	$nform['#suffix'] .= '</td>';
	
	if ($col == ($cols - 1)) {
	  $nform['#suffix'] .= '</tr>';
	  if ($row == ($rows - 1)) {
	    $nform['#suffix'] .= '</table>';
	  }
	}
	
	$form[] = $nform;
      }
    }
    //    $value .= '</table>';
    
    $walkinput = true;
    break;
    
  case 'inline':
    $type = 'markup';
    $value = $title;
    $walkinput = true;
    break;
    
  case 'text':
    $type = 'textfield';
    $size = 20;
    $cnt++;
    break;
    
  case 'message':
    if ($msg2drupal) {
      //form_set_error('', $input['value']);
      drupal_set_message($input['value']);
    } else {
      $type = 'markup';
    }
    break;
    
  case 'password':
    $type = 'password';
    $size = 20;
    $cnt++;
    break;
    
  case 'check':
    $type = 'checkbox';
    $cnt++;
    break;

  case 'display-image':
    $url = $input['value'];
    ewt_proxy_token($url,true);
    $type = 'markup';
    $value = '<img src="'.$url.'"/>';
    $cnt++;
    break;

  case 'image':
    /* find token, make allowed */
    $url = $input['baseURL'];
    $s = split('token=', $url);
    ewt_proxy_token($s[1],true);

    $type = 'markup';
    $value = '<img src="'.$url.'"/>';
    $cnt++;
    break;
    
  case 'date':
    $type = 'date_popup';
    $element['#date_format'] = "Y-m-d";
    if ($input['action']) {
      $attributes['onChange'] = 
	"document.getElementById('edit-ewt-action".$inst."').value='".$input['action']."'; document.getElementById('ewt-form".array_pop(split('-',$inst))."').submit();";
    }
    $cnt++;
    break;
    
  case 'button':
    $value = $input['label'];
    $type = 'button';
    if ($input['action']) {
      $type = 'submit';
      $attributes['onClick'] = 
	    "document.getElementById('edit-ewt-action".$inst."').value='".$input['action']."';";
    }
    $cnt++;
    break;
    
  default:
    if (is_array($input['option'])) {
      $options = array();
      if (isset($input['def'])) {
	$options[''] = $input['def'];
      }
      foreach ($input['option'] as $option) {
	$options[$option['value']] = $option['name'];
      }
      $type = 'select';
      if (isset($input['action'])) {
	$attributes['onChange'] = 
	  "document.getElementById('edit-ewt-action".$inst."').value='".$input['action']."'; document.getElementById('ewt-form".array_shift(split('-',$inst))."').submit();";
      }
      $cnt++;
    }
    break;
  }
  
  if ($type) {
    $element['#type'] = $type;

    if ($input['styleName']) {
      if ($type == 'markup') {
	$value = '<div class="'.$input['styleName'].'">'.$value.'</div>';
      } else {
	$attributes['class'] = $input['styleName'];
      }
    }

    if ($title)      $element['#title']      = t($title);
    if ($size)       $element['#size']       = $size;
    if ($attributes) $element['#attributes'] = $attributes;
    if ($options)    $element['#options']    = $options;
    if ($value)      $element['#value']      = $value;
    else             $element['#value']      = $values[$key];
    
    $element['#default_value'] = $element['#value'];
    
    if ($input['required'])
      $element['#required'] = ($input['required'] == true);

    if (isset($key))
      $form[$key] = $element;
    else
      $form[] = $element;
    
    if ($walkinput) {
      $nform = array('#type'=>'fieldset', '#tree'=>TRUE);
      $cnt += _ewtwalkset($nform, $values, $input, $inst);
      $cnt += _ewtwalk($nform, $values, $input, $inst);
      $form['inputset'] = $nform;
    }
  }
  return $cnt;
}

function _ewtwalkset(&$form, &$values, &$data, $inst) {
  $cnt = 0;
  if (is_array($data['inputset'])) {
    foreach ($data['inputset'] as $inputset) {
      $cnt += _ewtset($form, $values, $inputset, $inst);
    }
  }
  return $cnt;
}

function _ewtset(&$form, &$values, &$data, $inst) {
  $cnt = 0;
  $cnt += _ewtwalkset($form, $values, $data, $inst);

  if ($data['singleton'] == 'false') {

    $nform = array('#type'=>'fieldset', '#tree'=>TRUE);

    $form[] =
      array('#value' => $data,
	    '#theme' => 'ewt_table',
	    '#attributes' => $attributes,
	    );

    $cnt += _ewtwalk($nform, $values, $data, $inst);

  } else {

    $cnt += _ewtwalk($form, $values, $data, $inst);

  }
  return $cnt;
}

function ewt2drupalform($title, &$form, &$values, &$data, $inst) {

  $form['ewt']['#type']  = 'fieldset';
  $form['ewt']['#title'] = t($title);
  $form['ewt']['#tree']  = TRUE;

  $form['ewt']['action'.$inst] = 
    array('#type' => 'hidden', '#value' => '');

  _ewtset($form['ewt'], $values, $data, $inst);

  return $form;
}

function _ewtvalues($arr, &$res) {
  if (is_array($arr)) {
    foreach ($arr as $idx => $val) {
      if (is_array($val)) {
	_ewtvalues($val, $res);
      } else {
	$res[$idx] = $val;
      }
    }
    return true;
  }
  return false;
}

function ewtvalues(&$form_state, $res = array()) {

  _ewtvalues($form_state['values']['ewt'], $res);

  if (_ewtvalues($form_state['clicked_button']['#post'], $res))
    ;//$form_state['values']['ewt'] = $form_state['clicked_button']['#post']['ewt'];
  else
    if (_ewtvalues($form_state['post']['ewt'], $res))
      ;//$form_state['values']['ewt'] = $form_state['post']['ewt'];

  if (! _ewtvalues($form_state['clicked_button']['#post'], $res))
    _ewtvalues($form_state['post']['ewt'], $res);

  _ewtvalues($form_state['storage']['post'], $res);

  //var_dump($res);
  //var_dump($form_state);
  return $res;
}

function ewt_form(&$form_state, $service = 'drupalStatus', $title = 'EWT Form', 
		  $callback = false, $form = array(), $inst = '') {
error_log('ewt_form');
//  var_dump($form_state);

  $values = ewtvalues($form_state);

  $action = $values['action'];

  $return_form = $form_state[$service];

  if (!$return_form) {
    $data = ewt_action($action, $values, $service);

    if ($callback) $callback($data);

    // check for special meaning replies, like redirects
    if (is_array($data['changes'])) {
      foreach ($data['changes'] as $n => $v) {
	if (is_array($v)) {
	  if ($v['resource'] == 'redirect') {
	    $url = 'http://'.$_SERVER['SERVER_NAME'].$v['url'];
	    drupal_goto($url);
	  }
	}
      }
    }

    $return_form = ewt2drupalform($title, $form, $values, $data, $inst);

    $form_state[$service] = $return_form;
  }

  return $return_form;
}

function ewt_form_validate(&$form, &$form_state) {
error_log('ewt_form_validate');
//  var_dump($form);
 $form_state['storage']['post'] = $form['#post'];
 $form_state['rebuild'] = TRUE;
}

function ewt_form_submit(&$form, &$form_state) {
error_log('ewt_form_submit');
  $form_state['rebuild'] = TRUE;
}

function ewt_form2(&$form_state, $service = 'drupalStatus', $title = 'EWT Form', 
		   $callback = false, $form = array(), $inst = '') {
  return ewt_form($form_state, $service, $title, $callback, $form, '2'.$inst);
}

function ewt_form2_validate(&$form, &$form_state) {
  ewt_form_validate($form, $form_state);
}

function ewt_form2_submit(&$form, &$form_state) {
  ewt_form_submit($form, $form_state);
}

